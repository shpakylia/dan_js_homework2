let num, result = '';
do{
    num = prompt("Enter a number");
}while(isNaN(num) || !num);
for (let i=1; i<=num; i++){
    if(i%5 === 0){
        result += ' ' + i + ',';
    }

}
if(result.length === 0 ){
    result = 'Sorry, no numbers';
}
else{
    result = 'Numbers: '+ result;
}

console.log(result);